function transform_document(document)
	local re = '^(.*?) \\[(.*?)\\]\\s+(.*?)\\s+(.*?) - (.*)$'
	local m, err = ngx.re.match(document.message, re, 'moj')
	if m then
		local parsed = {
			timestamp = m[1],
			thread    = m[2],
			level     = m[3],
			logger    = m[4],
			message   = m[5]
		}
		document.parsed = parsed
	else
		if not err then
			ngx.log(ngx.WARN,
				string.format('Message not matched: %s', document.message))
		else
			ngx.log(ngx.ERR, 'Message match failed: ', err)
		end
	end
	return document
end
