-- restrict access

method = ngx.var.request_method
uri    = ngx.var.request_uri

if not ((method == 'POST' and uri == '/_bulk') or
		(method == 'GET'  and uri == '/')) then
	ngx.exit(ngx.HTTP_FORBIDDEN)
end

-- authenticate

api_key     = ngx.var.http_x_rdoctor_api_key
session_key = ngx.var.http_x_rdoctor_session_key

if not api_key or not session_key then
	ngx.exit(ngx.HTTP_UNAUTHORIZED)
end

redis = Redis:new()
ok, err = redis:connect(redis_host, redis_port)

if not ok then
	ngx.log(ngx.ERR, 'Failed to connect to Redis: ', err)
	ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
end

ok, err = redis:exists('apikey:' .. api_key)

if ok ~= 1 then
	if err then
		ngx.log(ngx.ERR, 'Failed to check API key in Redis: ', err)
		ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
	else
		ngx.exit(ngx.HTTP_UNAUTHORIZED)
	end
end

api_user, err = redis:get('apikey:' .. api_key)

if err then
	ngx.log(ngx.ERR, 'Failed to get API user from Redis: ', err)
	ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
end

ok, err = redis:exists('sessionkey:' .. session_key)

if ok ~= 1 then
	if err then
		ngx.log(ngx.ERR, 'Failed to get session key from Redis: ', err)
		ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
	else
		ngx.exit(ngx.HTTP_UNAUTHORIZED)
	end
end

-- replace destination index

if not (method == 'POST' and uri == '/_bulk') then
	return
end

session_index_line = string.format(index_line_fmt, session_key:lower())
new_lines = {}
source_line_next = false

for match in string.gmatch(ngx.req.get_body_data(), '([^\n]+)') do
	-- cjson throws exception on malformed input
	if source_line_next then
		local document = cjson.decode(match)

		document = transform_document(document)
		document.api_key     = api_key
		document.api_user    = api_user
		document.session_key = session_key

		local new_source_line = cjson.encode(document)
		table.insert(new_lines, new_source_line)
		source_line_next = false
	else
		local json = cjson.decode(match)
		if json['index'] and json['index']['_type'] == 'doc' then
			table.insert(new_lines, session_index_line)
			source_line_next = true
		end
	end
end

table.insert(new_lines, '')
ngx.req.set_body_data(table.concat(new_lines, '\n'))
