Redis = require "resty.redis"
cjson = require "cjson"

function string:starts_with(start)
	return self:sub(1, #start) == start
end

index_line_fmt = '{"index":{"_index":"s-%s","_type":"doc"}}'

redis_host = os.getenv('REDIS_HOST')
redis_port = os.getenv('REDIS_PORT')

if not redis_host or not redis_port then
	ngx.log(ngx.ERR, 'Redis host/port not defined')
	error('Redis host/port not defined')
end
