#!/bin/sh
set -e

: ${RNODE_DATA_DIR:?}
: ${RDOCTOR_API_KEY:?}
: ${RDOCTOR_SESSION_KEY:?}

: ${DATA_DIR:=/var/lib/rnurse}
: ${RDOCTOR_ELASTIC_URL:=https://woky.pyr8.io:9200}
: ${ELASTIC_VERSION:=6.5.0}
: ${FILEBEAT_DOCKER_IMAGE:=docker.elastic.co/beats/filebeat-oss:$ELASTIC_VERSION}

print_config_file() { cat <<EOF
setup.template.enabled: false
filebeat.inputs:
- type: log
  multiline:
    pattern: '^.*? \['
    negate: true
    match: after
  paths:
  - /var/lib/rnode/rnode.log

output.elasticsearch:
  hosts: ["$RDOCTOR_ELASTIC_URL"]
  headers:
    X-Rdoctor-Api-Key: "$RDOCTOR_API_KEY"
    X-Rdoctor-Session-Key: "$RDOCTOR_SESSION_KEY"
EOF
}

config_file=$(mktemp)
trap "rm $config_file" INT TERM QUIT
print_config_file >$config_file

docker run -it -u0 \
	--name=rnurse --rm \
	--network=host \
	-v $config_file:/usr/share/filebeat/filebeat.yml:ro \
	-v $DATA_DIR/filebeat/data:/usr/share/filebeat/data \
	-v $RNODE_DATA_DIR:/var/lib/rnode:ro \
	$FILEBEAT_DOCKER_IMAGE -strict.perms=false "$@"

rm $config_file
