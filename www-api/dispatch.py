import os
from pathlib import Path
import requests
import flask
from flask import request, session, g
import redis
from keygen import KeyGenerator

app = flask.Flask(__name__)

def read_secret(pathvar):
    file_path = os.environ[pathvar]
    key_text = Path(file_path).read_text()
    return key_text

def verify_captcha(request):
    payload = {
            'secret':   read_secret('RECAPTCHA_SECRET_FILE'),
            'response': request.form.get('g-recaptcha-response', ''),
            'remoteip': request.remote_addr
    }
    url = 'https://www.google.com/recaptcha/api/siteverify'
    resp = requests.get(url, payload)
    resp.raise_for_status()
    resp_msg = resp.json()
    if not resp_msg['success']:
        error_msg = 'reCAPTCHA verification failed: ' + \
                ', '.join(resp_msg['error-codes'])
        flask.abort(flask.Response(error_msg))

def create_keygen():
    return KeyGenerator.from_hex_masterkey(read_secret('MASTERKEY_FILE'))

def connect_to_redis():
    return redis.Redis.from_url(os.environ['REDIS_URL'])

@app.route('/api/getakey', methods = ['POST'])
def getakey_POST():
    verify_captcha(request)

    name = request.form.get('name', '')
    if not 'name' in request.form:
        return 'Missing or empty name argument', 400
    email = request.form.get('email', '')

    key_info = name
    if email:
        key_info += ' <' + email + '>'

    keygen = create_keygen()
    key = keygen.create_key(key_info).decode('ascii')

    redis_conn = connect_to_redis()
    redis_conn.set('apikey:' + key, key_info.encode('utf-8'))
    redis_conn.connection_pool.disconnect()

    return key
