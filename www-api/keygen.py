from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from base64 import b64encode, b64decode

class KeyGenerator:

    @classmethod
    def from_hex_masterkey(cls, masterkey_hex):
        return cls(bytes.fromhex(masterkey_hex))

    def __init__(self, masterkey):
        self.masterkey = masterkey

    def create_key(self, info):
        cipher = AES.new(self.masterkey, AES.MODE_GCM)
        ciphertext, tag = cipher.encrypt_and_digest(info.encode('utf-8'))
        return b64encode(cipher.nonce + tag + ciphertext)

    def get_key_info(self, key):
        key_bytes = b64decode(key)
        nonce, tag = key_bytes[0:16], key_bytes[16:32]
        ciphertext = key_bytes[32:]
        cipher = AES.new(self.masterkey, AES.MODE_GCM, nonce)
        info_bytes = cipher.decrypt_and_verify(ciphertext, tag)
        return info_bytes.decode('utf-8')
